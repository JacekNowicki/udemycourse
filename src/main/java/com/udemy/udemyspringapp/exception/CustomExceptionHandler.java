package com.udemy.udemyspringapp.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class CustomExceptionHandler{
	@ExceptionHandler(CustomException.class)
	public ResponseEntity<Object> customResponse() {		
		return new ResponseEntity(new CustomResponse("My Custom Response"), HttpStatus.INTERNAL_SERVER_ERROR);		
	}
	

}
