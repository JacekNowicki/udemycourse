package com.udemy.udemyspringapp.exception;

public class CustomResponse {
	
	private String msg;

	public CustomResponse(String msg) {	
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

}
