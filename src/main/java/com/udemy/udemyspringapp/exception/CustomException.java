package com.udemy.udemyspringapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class CustomException extends RuntimeException {
	
	public CustomException(String message) {
		super(message);		
	}
	

}
