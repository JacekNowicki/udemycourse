package com.udemy.udemyspringapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UdemySpringAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(UdemySpringAppApplication.class, args);
	}

}
