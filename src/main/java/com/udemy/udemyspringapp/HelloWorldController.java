package com.udemy.udemyspringapp;

import java.net.URI;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.udemy.udemyspringapp.exception.CustomException;

@RestController
public class HelloWorldController {

//	private final String getUri = "/hello-world/{x}";			

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/hello-world/{x}")
//	@GetMapping(getUri)
	public ResponseEntity helloWorld(@PathVariable int x) {			
//		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{}").buildAndExpand(x).toUri();
//		return ResponseEntity.created(uri).build();
		if (x == 0) throw new CustomException("No entity with id '0' !");
		return new ResponseEntity("Hello World!!!!! "+x, HttpStatus.CREATED);
	}
	
}
